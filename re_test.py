#!/usr/bin/python

from re_test_patterns import test_patterns

test_patterns('aaaaaaaaaaaaaaaaaaa',
              [ 'a(ab)'   ,
                'a(a*b*)' , 
                'a(ab)*'  ,
                'a(ab)+'
            ]
          )


